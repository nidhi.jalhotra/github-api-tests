# Getting started with Serenity and Cucumber

This project show you how to get started with REST-API testing using Serenity and Cucumber and RestAssured. 

## Before cloning this project you need:
```
- JDK 11 which you can download from: https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
- Gradle 6.9.1
```


## Clone with HTTPS

Git:

   git clone git@gitlab.com:nidhi.jalhotra/github-api-tests.git

## The project

This project gives you a basic project setup, along with some RestAssured-based tests (CRUD and Negative Scenario) 

### The project directory structure
The project has build scripts for Gradle and follows the below structure:
```
src
  + main
  + test
    + java                          Test runner,Steps and steps defintions supporting cucumber scenarios
    + resources
      + features                          Feature files

```

## A simple CRUD feature file along with a Negative scenario with the Github API
The project comes with four feature file, which contains a basic CRUD functionality

This sample project uses Cucumber scenarios while exercising the Github API `https://api.github.com` 
of a hypothetical user who is performing a **C** (Create a repository) **R** (Getting all the repositories) **U** (Updating the name of the repository) **D** (Deleting the repository) use case, 
after he first tried to create a reposirory with  **unauthorized user**  

```Gherkin
 Scenario: Create a user project with an authenticated user
    Given user creates a project with name "sample11" with authentication
    Then the api responds with status code 201
    And the "sample11" project should be created
```

The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.
Example of the Glue code:
```java
  @Given("user creates a project with name {string} with authentication")
    public void userCreatesAProjectWithNameWithAuthentication(String projectName) {
        project.create(projectName);
        currentProjectName = projectName;
    }

    @And("the {string} project should be created")
    public void theProjectShouldBeCreated(String projectName) {
        project.validateProjectCreated(projectName);
    }
```

The actual REST calls are performed using RestAssured in the ProjectSteps helper, like here: 

```java
  public void get() {
        response = given().contentType("application/json")
                .header("Authorization","Bearer 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")
                .baseUri("https://api.github.com")
                .basePath("/user/repos")
                .when().get();
        response.prettyPrint();
    }
```

## Executing the tests & Living documentation

- From the root folder of the project, run `./gradlew` to run the tests, and find the generated report in:
```
target
  + site
    + serenity
      + index.html                           
```

- In the pipeline, reports can be accessed from the GitLab pipeline from the `Artifacts` section of the job. The path to the report is `target/site/serenity/index.html`


## To run the specific tests for regression -

- Create - `./gradlew -Dcucumber.options="--tags '@create'"`
- Read -`./gradlew -Dcucumber.options="--tags '@get'"`
- Update - `./gradlew -Dcucumber.options="--tags '@update'"`
- Delete -`./gradlew -Dcucumber.options="--tags '@delete'"`
- Regression (to run all the tests) - `./gradlew -Dcucumber.options="--tags '@regression'"`

# CI/CD

Gitlab-CI integration is available [here](https://gitlab.com/nidhi.jalhotra/github-api-tests/-/pipelines).







