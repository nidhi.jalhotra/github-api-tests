@delete @regression
Feature: Delete a repository

  Scenario: Delete an existing repository
    Given user creates a project with name "sample200" with authentication
    When user deletes the project "sample200"
    Then the api responds with status code 204

  @cleanup
  Scenario: Delete an non-existing repository
    Given user creates a project with name "sample200" with authentication
    When user deletes the project "sample201"
    Then the api responds with status code 404
