@get @regression
Feature: Get list of Github Projects

  @cleanup
  Scenario: Get list of projects of an authenticated user
    Given user creates a project with name "sample7" with authentication
    When user gets the list of projects
    Then the api responds with status code 200


