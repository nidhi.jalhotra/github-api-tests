@create @regression
Feature: Create Github Project

  @cleanup
  Scenario: Create a user project with an authenticated user
    Given user creates a project with name "sample11" with authentication
    Then the api responds with status code 201
    And the "sample11" project should be created

  Scenario: Create a user project with an unauthenticated user
    When user creates a project with name "sample" without authentication
    Then the api responds with status code 401
