@update @regression
Feature: Update a Github Project

  @cleanup
  Scenario: Update the name of the github project
    Given user creates a project with name "sample9" with authentication
    When user updates name of the project from "sample9" to "sample10"
    Then the api responds with status code 200

  @cleanup
  Scenario: Update the name of the github project
    Given user creates a project with name "sample9" with authentication
    When user updates name of the project from "sample400" to "sample10"
    Then the api responds with status code 404