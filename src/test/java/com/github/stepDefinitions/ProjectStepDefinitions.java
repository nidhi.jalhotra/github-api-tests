package com.github.stepDefinitions;

import com.github.steps.ProjectSteps;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ProjectStepDefinitions {

    String currentProjectName;

    @Steps
    ProjectSteps project;

    @Given("user creates a project with name {string} with authentication")
    public void userCreatesAProjectWithNameWithAuthentication(String projectName) {
        project.create(projectName);
        currentProjectName = projectName;
    }

    @And("the {string} project should be created")
    public void theProjectShouldBeCreated(String projectName) {
        project.validateProjectCreated(projectName);
    }

    @Then("the api responds with status code {int}")
    public void theApiRespondsWithStatusCode(int code) {
        project.validateResponseToBe(code);
    }

    @When("user gets the list of projects")
    public void userGetsTheListOfProjects() {
        project.get();
    }

    @When("user updates name of the project from {string} to {string}")
    public void userUpdatesNameOfTheProjectFromTo(String oldProjectName, String newProjectName) {
        project.update(oldProjectName, newProjectName);
        currentProjectName = newProjectName;
    }

    @When("user deletes the project {string}")
    public void userDeletesTheProject(String projectName) {
        project.delete(projectName);
    }

    @When("user creates a project with name {string} without authentication")
    public void userCreatesAProjectWithNameWithoutAuthentication(String projectName) {
        project.createWithoutAuthentication(projectName);
    }

    @After(value="@cleanup")
    public void cleanUp() {
        project.delete(currentProjectName);
    }
}
