package com.github.steps;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;

public class Utils {
    private EnvironmentVariables environmentVariables;

    @Step
    public String getUrl() {
        return EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("github.api.url");
    }

    @Step
    public String getGitHubApiKey() {
        return System.getenv("GITHUB_API_KEY");
    }
}
