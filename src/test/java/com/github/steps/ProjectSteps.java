package com.github.steps;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class ProjectSteps {
    Response response;

    @Steps
    Utils utils;

    @Step
    public void create(String name) {
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("name", name);

        response = given().contentType("application/json")
                .header("Authorization",String.format("Bearer %s",utils.getGitHubApiKey()))
                .body(jsonAsMap)
                .baseUri(utils.getUrl())
                .basePath("/user/repos")
                .when().post();
    }

    @Step
    public void createWithoutAuthentication(String name) {
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("name", name);

        response = given().contentType("application/json")
                .body(jsonAsMap)
                .baseUri(utils.getUrl())
                .basePath("/user/repos")
                .when().post();
    }

    @Step
    public void delete(String name) {
        response = given().contentType("application/json")
                .header("Authorization",String.format("Bearer %s",utils.getGitHubApiKey()))
                .baseUri(utils.getUrl())
                .basePath(String.format("/repos/nniddhiii/%s", name))
                .when().delete();
    }

    @Step
    public void update(String oldName, String newName) {
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("name", newName);

        response = given().contentType("application/json")
                .header("Authorization",String.format("Bearer %s",utils.getGitHubApiKey()))
                .body(jsonAsMap)
                .baseUri(utils.getUrl())
                .basePath(String.format("/repos/nniddhiii/%s", oldName))
                .when().patch();
    }

    @Step
    public void get() {
        response = given().contentType("application/json")
                .header("Authorization",String.format("Bearer %s",utils.getGitHubApiKey()))
                .baseUri(utils.getUrl())
                .basePath("/user/repos")
                .when().get();
    }

    @Step
    public void validateResponseToBe(int code) {
        assertThat(code).isEqualTo(response.getStatusCode());
    }

    @Step
    public void validateProjectCreated(String projectName) {
        assertThat(projectName).isEqualTo(response.jsonPath().get("name"));
    }
}
